import java.util.Scanner;

/**
 * Created by Тоша on 07.06.2017.
 */
public class Ex12 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] string) {
        System.out.print("Введите количество суток: ");
        int day = scanner.nextInt();
        int hours = 24 * day;
        int minutes = hours * 60;
        int seconds = minutes * 60;
        System.out.println("В " + day + " суток " + hours + " часов; " + minutes + " минут; " + seconds + " секунд.");

    }
}