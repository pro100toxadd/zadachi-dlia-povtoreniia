import java.util.Scanner;

/**
 * Created by Тоша on 22.06.2017.
 */
public class Ex6 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число: ");
        int number = scanner.nextInt();
        int maxLimit = ((number <= 10) ? 11 : number + 1);
        for (int i = 1; i < maxLimit; i++ ) {
            System.out.println(number + " x " + i + " = " + (number * i) + ":");
        }
    }
}