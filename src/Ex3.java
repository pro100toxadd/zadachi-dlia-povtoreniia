import java.util.Scanner;

/**
 * Created by Тоша on 06.06.2017.
 */
public class Ex3 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        double inputMoney;
        do {
            System.out.print("Рубли: ");
            inputMoney = scanner.nextDouble();
        } while (inputMoney <= 0);
        double convMlt;

        do {
            System.out.print("Цена евро: ");
            convMlt = scanner.nextDouble();
        } while (convMlt <= 0);
        System.out.println("Результат: " + Convertor(inputMoney,1/convMlt) + " евро.");
    }

    private static double Convertor(double inValute, double conversionMultipler) {
        return inValute * conversionMultipler;
    }
}