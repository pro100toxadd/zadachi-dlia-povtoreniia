/**
 * Created by Тоша on 06.06.2017.
 */
public class Ex5 {

    public static void main(String[] args) {
        for (int i = 2; i < 101; i++) {
            if (checkPrime(i)) {
                System.out.println(i + " Простое");

            } else{
                System.out.println(i + " Не простое");
            }
        }
    }
    private static boolean checkPrime(int number) {
        boolean result = true;
        for (int i = 2; i < number && result; i++) {
            result = number % i != 0;
        }
        return result;
    }
}