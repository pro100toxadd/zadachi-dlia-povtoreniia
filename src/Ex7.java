import java.util.Scanner;

/**
 * Created by Тоша on 22.06.2017.
 */
public class Ex7 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        int number = (int) random(1,10);
        int input;
        while (true) {
            System.out.print("Введите число (от 1 до 10)");
            input = scanner.nextInt();
            if (input > number) {
                System.out.println("Число больше загаданного.");
            } else if (input < number) {
                System.out.println("Число меньше загаданного.");
            } else if (input == number) {
                System.out.println("Правильно.");
                return;
            }
        }
    }
    static double random(double min, double max) {
        return min + (Math.random()*(max-min+1));
    }
}