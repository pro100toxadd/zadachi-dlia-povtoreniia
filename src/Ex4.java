/**
 * Created by Тоша on 06.06.2017.
 */
public class Ex4 {
    final private static double delay = 6.8; //Секунды
    final private static int speedOfSound = 331; //Метры и секунды

    public static void main(String[] args) {
        System.out.print("Расстояние от места, куда ударила молния до места, где был услышан гром, если интервал между " +
                "вспышкй и звуком был равен " + delay + " секунл: " + formatPrefixToBigNumbers((int) delay * speedOfSound, "метров"));
    }

    private static String formatPrefixToBigNumbers(double number, String nameOfUnit) {
        String prefix = "";
        int multiplier = 1;
        if (number / 1000 >= 1) {
            prefix = "Кило";
            multiplier = 1000;
        }
        return (number / multiplier) + " " + prefix + nameOfUnit;
    }
}