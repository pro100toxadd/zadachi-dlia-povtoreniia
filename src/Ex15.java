/**
 * Created by Тоша on 23.06.2017.
 */
import java.util.Arrays;
public class Ex15 {

    public static void main(String[] args) {
        int[][] matrix = new int[(int)randomRangeGen(2,5)][(int)randomRangeGen(2,5)];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                matrix[i][j] = (int)randomRangeGen(-10,10);
            }
        }
        for (int i = 0; i < matrix.length; i++) {
            System.out.println(Arrays.toString(matrix[i]));
        }
        System.out.println("---------------------------------------------");
        matrix = transposeMatrix(matrix);
        for (int i = 0; i < matrix.length; i++) {
            System.out.println(Arrays.toString(matrix[i]));
        }
    }

    static int[][] transposeMatrix(int[][] matrix) {
        int[][] tempMatrix;
        //Проверка на рваную матрицу
        int h = matrix.length;
        int w = 0;
        if (h != 0) {
            w = matrix[0].length;
            for (int i = 1; i < matrix.length && w != 0; i++) {
                if (matrix[i].length != w) {
                    w = 0;
                }
            }
        }
        tempMatrix = new int[w][h];
        for (int j = 0; j < matrix[0].length; j++) {
            for (int i = 0; i < matrix.length; i++){
                tempMatrix[j][i] = matrix[i][j];
            }
        }
        return tempMatrix;
    }

    //Метод генерации случайного числа стыренный у Кирилла.
    static double randomRangeGen(double min, double max) {
        return min + (Math.random()*(max-min+1));
    }
}