import java.util.Scanner;

/**
 * Created by Тоша on 23.06.2017.
 */
public class Ex14 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            System.out.print("Введите символ: ");
            System.out.println("Этот символ является \"" + getSymbolType(scanner.next().charAt(0)) + "\".");
        }
    }

      //Определяет чем является символ

    static SymbolType getSymbolType(char symbol) {
        final String punctuationSymbols = ".,!?;\"#№$%^:&*()/+-=_`'\\<>{}[]|";
        final String digits = "0123456789";
        SymbolType result = SymbolType.Буквой;
        for (int i = 0; i < punctuationSymbols.length() && result == SymbolType.Буквой; i++) {
            if (punctuationSymbols.charAt(i) == symbol) {
                result = SymbolType.Пунктуационным_символом;
            }
        }
        for (int i = 0; i < digits.length() && result == SymbolType.Буквой; i++) {
            if (digits.charAt(i) == symbol) {
                result = SymbolType.Числом;
            }
        }
        return result;
    }

    enum SymbolType {
        Числом, Буквой, Пунктуационным_символом
    }
}