import java.util.Arrays;
import java.util.Scanner;
/**
 * Created by Тоша on 06.06.2017.
 */
public class Ex2 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        double[] exampleArr = {0.3,23,75,9.85,6.931};
        System.out.println("Array: " + Arrays.toString(exampleArr));
        System.out.print("Номер массива элемента, который нужно увеличить: ");
        int index = scanner.nextInt();
         if (exampleArr.length >= index && index >= 0) {
            exampleArr[index] += exampleArr[index] / 100 * 10;
         }
        System.out.println("Modified array: " + Arrays.toString(exampleArr));
    }
}