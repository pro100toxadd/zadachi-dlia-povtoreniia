import java.util.Scanner;

/**
 * Created by Тоша on 07.06.2017.
 */
public class Ex8 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введи число ");
        double inputNumber = scanner.nextDouble();
        if (inputNumber != (int) inputNumber) {
            System.out.println("Это дробное число.");
        } else {
            System.out.println("Это целое число.");
        }
    }
}