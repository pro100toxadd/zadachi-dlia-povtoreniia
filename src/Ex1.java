import java.util.Scanner;

/**
 * Created by Тоша on 06.06.2017.
 */
public class Ex1 {

    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        String example = scanner.nextLine();
        int n = example.indexOf('.');
        String output = example;
        if (n > -1) {
            output = example.substring(0, n);
        }
        int j = 0;
        for (int i = 0; i < output.length(); i++) {
            if (output.charAt(i) == ' ') {
                j++;
            }
        }
        System.out.println("Введенное вами: " + example + "\n Вывод: " + output + "\nКоличество пробелов: " + j);
    }

}